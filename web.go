package main

import (
	"encoding/json"
	"net/http"
	"os"
	"time"
)

type WebResponse struct {
	Success      bool      `json:"success"`
	MOTD         string    `json:"motd,omitempty"`
	ErrorMessage []string  `json:"error,omitempty"`
	CurrentTime  time.Time `json:"currentDate,omitempty"`
	Servername   string    `json:"serverName,omitempty"`
}

type WebRequest struct {
	Topic string `json:"topic"`
	Value string `json:"value"`
	Delay int    `json:"delay"` // to make program go slower
}

func handlePost(w http.ResponseWriter, r *http.Request) {
	hostname, _ := os.Hostname()
	response := WebResponse{
		Success:      false,
		MOTD:         "Life is what happens when you're busy making other plans.",
		CurrentTime:  time.Now(),
		Servername:   hostname,
		ErrorMessage: nil,
	}
	w.Header().Set("Content-Type", "application/json")
	// parse json
	var input WebRequest
	err := json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		response.ErrorMessage = append(response.ErrorMessage, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
	}
	// check parameters
	if len(input.Topic) == 0 || len(input.Value) == 0 {
		w.WriteHeader(http.StatusInternalServerError)
		response.ErrorMessage = append(response.ErrorMessage, "Missing parameters in body")
	}
	// send message if not any errors
	if len(response.ErrorMessage) == 0 {
		err = PushMessage(input.Value, input.Topic)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			response.ErrorMessage = append(response.ErrorMessage, err.Error())
		} else {
			response.Success = true
		}
	}
	bytes, _ := json.Marshal(response)
	w.Write(bytes)
	// make delay
	if input.Delay > 0 {
		time.Sleep(time.Duration(input.Delay))
	}
}

func handleGet(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		handlePost(w, r)
		return
	}
	if r.Method != http.MethodGet {
		return
	}
	hostname, _ := os.Hostname()
	response := WebResponse{
		Success:      false,
		MOTD:         "The way to get started is to quit talking and begin doing.",
		CurrentTime:  time.Now(),
		Servername:   hostname,
		ErrorMessage: nil,
	}
	params := r.URL.Query()
	topic := params.Get("topic")
	msg := params.Get("value")
	w.Header().Set("Content-Type", "application/json")
	// see if we have a topic
	if len(topic) == 0 {
		w.WriteHeader(http.StatusInternalServerError)
		response.ErrorMessage = append(response.ErrorMessage, "Missing topic")
	}
	// see if we have a message
	if len(msg) == 0 {
		w.WriteHeader(http.StatusInternalServerError)
		response.ErrorMessage = append(response.ErrorMessage, "Missing value")
	}
	var err error
	if len(response.ErrorMessage) == 0 {
		err = PushMessage(msg, topic)
		response.Success = true
	}
	if err != nil {
		response.ErrorMessage = append(response.ErrorMessage, err.Error())
		response.Success = false
	}
	bytes, _ := json.Marshal(response)
	w.Write(bytes)
}
