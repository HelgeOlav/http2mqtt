FROM golang:alpine AS build
WORKDIR /src
copy . .
RUN go build

FROM alpine:latest AS bin
COPY --from=build /src/http2mqtt /
RUN apk update
EXPOSE 80

ENTRYPOINT ["/http2mqtt"]
