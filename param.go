package main

import (
	"github.com/spf13/viper"
)

// get params

var MQTT_Server string
var HTTP_port int

func params() {
	viper.SetDefault("port", 80)
	viper.SetDefault("mqtt", "tcp://10.1.11.2:1883")
	viper.BindEnv("mqtt")
	viper.BindEnv("port")

	HTTP_port = viper.GetInt("port")
	MQTT_Server = viper.GetString("mqtt")

	// check parameter
}
