package main

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"time"
)

var MQTT mqtt.Client

func DisconnectMQTT() {
	// check if we are connected and bail out if so
	if MQTT != nil && MQTT.IsConnected() {
		return
	}
	msg := fmt.Sprintf("disconnect %v", time.Now())
	err := PushMessage(msg, infoTopic)
	if err != nil {
		fmt.Printf("ConnectMessage failed: %v\n", err)
	}
	time.Sleep(time.Second)
	MQTT.Disconnect(250)
	MQTT = nil
}

// Connect to the MQTT broker
func ConnectMQTT() {
	// check if we are connected and bail out if so
	if MQTT != nil && MQTT.IsConnected() {
		return
	}
	// set up connection
	opts := mqtt.NewClientOptions()
	opts.AddBroker(MQTT_Server)
	opts.AutoReconnect = true
	opts.ConnectRetry = true
	opts.KeepAlive = 900
	opts.SetClientID("golang-server")
	MQTT = mqtt.NewClient(opts)
	// connect
	token := MQTT.Connect()
	if token.Error() != nil {
		fmt.Printf("connect error: %v\n", token.Error())
	}
	time.Sleep(time.Second)
}

// Send a message out
func PushMessage(msg interface{}, topic string) error {
	ConnectMQTT()
	token := MQTT.Publish(topic, 0, false, msg)
	token.Wait()
	return token.Error()
}

const infoTopic = "shellypush2"

// Send connect message
func ConnectMessage() {
	msg := fmt.Sprintf("connected %v", time.Now())
	err := PushMessage(msg, infoTopic)
	if err != nil {
		fmt.Printf("ConnectMessage failed: %v\n", err)
	}
}

// Test push some messages
func SendTestMessages(num int, delay time.Duration) {
	for x := 1; x < num; x++ {
		msg := fmt.Sprintf("Message %v @ %v", x, time.Now())
		err := PushMessage(msg, "helgeo")
		if err != nil {
			fmt.Println(err)
		}
		time.Sleep(delay)
	}
}
