package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	params()
	fmt.Printf("MQTT: %v, listen port %v\n", MQTT_Server, HTTP_port)
	// mqtt.DEBUG = log.New(os.Stdout, "", 0)
	ConnectMQTT()
	ConnectMessage()
	time.Sleep(time.Second)
	go SendTestMessages(300, time.Second*3)
	// set up web
	http.HandleFunc("/message", handleGet)
	listenPort := fmt.Sprintf(":%v", HTTP_port)
	_ = http.ListenAndServe(listenPort, nil)
	DisconnectMQTT() // should never be called
}
