# Introduction

This package accepts HTTP GET calls and translates them into MQTT messages published to a topic.

```curl http://server:8888/message?topic=my/topic&value=OK```

You can use the following environment variables:
PORT=portnumer for http server
MQTT=tcp://host:port for MQTT server